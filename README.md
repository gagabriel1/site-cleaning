Site Cleaning Simulator
============

System Requirement
------------
* Maven (only to build the application, has only been tested with version 3.x)
* Java 8 (should work with newer versions but it has not been tested)

How to Run
------------

Run
```
$ mvn clean package
```
to run all tests and build the application.

The above will generate a JAR.

To run the JAR:
```
$ java -jar target/site-cleaning.jar <path to file>
```
where ```<path to file>``` should be the path relative to the invocation directory

Code Considerations
------------

* Comments have only been added where they add some relevant info to the method, 
effort was put into making the method name self explanatory
