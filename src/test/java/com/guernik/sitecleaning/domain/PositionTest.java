package com.guernik.sitecleaning.domain;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class PositionTest {

    @Test
    public void plusValidMovement() {
        assertThat(new Position(1, 2).plus(new Movement(2, 3)),
                equalTo(new Position(3, 5)));
    }

    @Test
    public void plusMovementWithNegativeCoordinates() {
        assertThat(new Position(1, 2).plus(new Movement(-2, -3)),
                equalTo(new Position(-1, -1)));
    }

    @Test
    public void plusZeroMovement() {
        assertThat(new Position(1, 2).plus(new Movement(0, 0)),
                equalTo(new Position(1, 2)));
    }
}