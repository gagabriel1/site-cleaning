package com.guernik.sitecleaning.domain;

import org.junit.Test;

import static com.guernik.sitecleaning.domain.SiteSquareState.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class SiteTest {

    @Test
    public void initializeFromValidState() {
        SiteSquareState[][] givenState = {{CLEARABLE_TREE, ROCKY, PLAIN},
                {PLAIN, PROTECTED_TREE, CLEARABLE_TREE}};
        Site site = new Site(givenState);
        assertThat(site.getState(), equalTo(givenState));
    }

    @Test(expected = IllegalArgumentException.class)
    public void initializeFromNullShouldFail() {
        SiteSquareState[][] givenState = null;
        new Site(givenState);
    }

    @Test(expected = IllegalArgumentException.class)
    public void initializeFromEmptyShouldFail() {
        SiteSquareState[][] givenState = {};
        new Site(givenState);
    }

    @Test(expected = IllegalArgumentException.class)
    public void initializeFromJaggedArrayShouldFail() {
        SiteSquareState[][] givenState = {{CLEARABLE_TREE, ROCKY},
                {PLAIN}};
        new Site(givenState);
    }

    @Test(expected = IllegalArgumentException.class)
    public void initializeFromArrayWithNullRowShouldFail() {
        SiteSquareState[][] givenState = {{CLEARABLE_TREE, ROCKY},
                null};
        new Site(givenState);
    }

    @Test(expected = IllegalArgumentException.class)
    public void initializeFromArrayWithNullFirstRowShouldFail() {
        SiteSquareState[][] givenState = {null,
                {CLEARABLE_TREE, ROCKY}};
        new Site(givenState);
    }

    @Test(expected = IllegalArgumentException.class)
    public void initializeFromArrayWithNullElementShouldFail() {
        SiteSquareState[][] givenState = {{CLEARABLE_TREE, ROCKY},
                {CLEARABLE_TREE, null}};
        new Site(givenState);
    }

    @Test
    public void clearValidPosition() {
        SiteSquareState[][] givenState = {{CLEARABLE_TREE, ROCKY, PLAIN},
                {PLAIN, PROTECTED_TREE, CLEARABLE_TREE}};
        Site site = new Site(givenState);

        assertThat(site.clear(new Position(0, 0)), equalTo(CLEARABLE_TREE));

        assertThat(site.clear(new Position(0, 0)), equalTo(CLEARED));
    }

    @Test(expected = OutOfBoundsException.class)
    public void clearPositionOutsideSite() {
        SiteSquareState[][] givenState = {{CLEARABLE_TREE, ROCKY, PLAIN},
                {PLAIN, PROTECTED_TREE, CLEARABLE_TREE}};
        Site site = new Site(givenState);

        site.clear(new Position(1, 3));
    }

    @Test
    public void protectedTreeCanBeCleared() {
        SiteSquareState[][] givenState = {{CLEARABLE_TREE, ROCKY, PLAIN},
                {PLAIN, PROTECTED_TREE, CLEARABLE_TREE}};
        Site site = new Site(givenState);

        assertThat(site.clear(new Position(1, 1)), equalTo(PROTECTED_TREE));
    }

    @Test
    public void countSquaresForType() {
        SiteSquareState[][] givenState = {{CLEARABLE_TREE, ROCKY, PLAIN},
                {PLAIN, PROTECTED_TREE, CLEARABLE_TREE}};
        Site site = new Site(givenState);

        assertThat(site.amountOfSquaresFor(CLEARABLE_TREE), equalTo(2));
        assertThat(site.amountOfSquaresFor(PLAIN), equalTo(2));
        assertThat(site.amountOfSquaresFor(PROTECTED_TREE), equalTo(1));
        assertThat(site.amountOfSquaresFor(ROCKY), equalTo(1));
        assertThat(site.amountOfSquaresFor(CLEARED), equalTo(0));
    }

    @Test
    public void unclearedSquares() {
        SiteSquareState[][] givenState = {{CLEARED, ROCKY},
                {PLAIN, CLEARABLE_TREE}};
        Site site = new Site(givenState);

        assertThat(site.unclearedSquares(), equalTo(3));
    }

    @Test
    public void unclearedSquaresDoesNotIncludeProtectedTree() {
        SiteSquareState[][] givenState = {{PROTECTED_TREE, PLAIN, CLEARED}};
        Site site = new Site(givenState);

        assertThat(site.unclearedSquares(), equalTo(1));
    }
}