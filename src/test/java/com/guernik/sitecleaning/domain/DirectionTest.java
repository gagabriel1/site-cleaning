package com.guernik.sitecleaning.domain;

import org.junit.Test;

import static com.guernik.sitecleaning.domain.Direction.*;
import static com.guernik.sitecleaning.domain.Rotation.LEFT;
import static com.guernik.sitecleaning.domain.Rotation.RIGHT;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class DirectionTest {

    @Test
    public void rotateToRight() {
        assertThat(NORTH.rotate(RIGHT), equalTo(EAST));
        assertThat(EAST.rotate(RIGHT), equalTo(SOUTH));
        assertThat(SOUTH.rotate(RIGHT), equalTo(WEST));
        assertThat(WEST.rotate(RIGHT), equalTo(NORTH));
    }

    @Test
    public void rotateToLeft() {
        assertThat(NORTH.rotate(LEFT), equalTo(WEST));
        assertThat(WEST.rotate(LEFT), equalTo(SOUTH));
        assertThat(SOUTH.rotate(LEFT), equalTo(EAST));
        assertThat(EAST.rotate(LEFT), equalTo(NORTH));
    }
}