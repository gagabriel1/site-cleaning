package com.guernik.sitecleaning.domain;

import org.junit.Before;
import org.junit.Test;

import static com.guernik.sitecleaning.domain.Direction.*;
import static com.guernik.sitecleaning.domain.Rotation.LEFT;
import static com.guernik.sitecleaning.domain.Rotation.RIGHT;
import static com.guernik.sitecleaning.domain.SiteSquareState.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class BulldozerTest {

    private Site site;

    @Before
    public void setUpSite() {
        SiteSquareState[][] givenState = {{CLEARABLE_TREE, ROCKY, PLAIN},
                {PLAIN, CLEARABLE_TREE, CLEARABLE_TREE},
                {CLEARABLE_TREE, ROCKY, PLAIN}};
        site = new Site(givenState);
    }

    @Test
    public void initialPositionAndDirection() {
        Bulldozer bulldozer = new Bulldozer(site);
        assertThat(bulldozer.getPosition(), equalTo(new Position(-1, 2)));
        assertThat(bulldozer.getDirection(), equalTo(EAST));
    }

    @Test
    public void advanceOneIntoSite() {
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(1);
        assertThat(bulldozer.getPosition(), equalTo(new Position(0, 2)));
        assertThat(bulldozer.getDirection(), equalTo(EAST));
    }

    @Test
    public void advanceTwoIntoSite() {
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(2);
        assertThat(bulldozer.getPosition(), equalTo(new Position(1, 2)));
        assertThat(bulldozer.getDirection(), equalTo(EAST));
    }

    @Test
    public void turnRight() {
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(2);
        bulldozer.turn(RIGHT);
        assertThat(bulldozer.getDirection(), equalTo(SOUTH));
    }

    @Test
    public void advanceSouth() {
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(2);
        assertThat(bulldozer.getPosition(), equalTo(new Position(1, 2)));
        bulldozer.turn(RIGHT);
        bulldozer.advance(1);
        assertThat(bulldozer.getPosition(), equalTo(new Position(1, 1)));
        assertThat(bulldozer.getDirection(), equalTo(SOUTH));
    }

    @Test
    public void turnLeft() {
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(2);
        bulldozer.turn(LEFT);
        assertThat(bulldozer.getDirection(), equalTo(NORTH));
    }

    @Test(expected = OutOfBoundsException.class)
    public void shouldNotMoveOutsideOfSite() {
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(2);
        bulldozer.turn(LEFT);
        assertThat(bulldozer.getDirection(), equalTo(NORTH));
        bulldozer.advance(1);
    }

    @Test
    public void fuelConsumptionForPlainLand() {
        Bulldozer bulldozer = new Bulldozer(singleSquareSiteFor(PLAIN));
        bulldozer.advance(1);
        assertThat(bulldozer.getConsumedFuel(), equalTo(1));
    }

    @Test
    public void fuelConsumptionForRockyLand() {
        Bulldozer bulldozer = new Bulldozer(singleSquareSiteFor(ROCKY));
        bulldozer.advance(1);
        assertThat(bulldozer.getConsumedFuel(), equalTo(2));
    }

    @Test
    public void fuelConsumptionForClearableTreeLand() {
        Bulldozer bulldozer = new Bulldozer(singleSquareSiteFor(CLEARABLE_TREE));
        bulldozer.advance(1);
        assertThat(bulldozer.getConsumedFuel(), equalTo(2));
    }

    @Test
    public void fuelConsumptionForProtectedTreeLand() {
        Bulldozer bulldozer = new Bulldozer(singleSquareSiteFor(PROTECTED_TREE));
        try {
            bulldozer.advance(1);
        } catch (ProtectedTreeClearedException e) {
            // expected
        }
        assertThat(bulldozer.getConsumedFuel(), equalTo(2));
    }

    @Test
    public void fuelConsumptionForClearedLand() {
        Bulldozer bulldozer = new Bulldozer(singleSquareSiteFor(CLEARED));
        bulldozer.advance(1);
        assertThat(bulldozer.getConsumedFuel(), equalTo(1));
    }

    @Test
    public void fuelConsumptionForLongAdvance() {
        Bulldozer bulldozer = new Bulldozer(singleLineSiteFor(ROCKY, PLAIN));
        bulldozer.advance(2);
        assertThat(bulldozer.getConsumedFuel(), equalTo(3));
    }

    @Test
    public void fuelConsumptionForTwoAdvances() {
        Bulldozer bulldozer = new Bulldozer(singleLineSiteFor(ROCKY, PLAIN));
        bulldozer.advance(1);
        bulldozer.advance(1);
        assertThat(bulldozer.getConsumedFuel(), equalTo(3));
    }

    @Test
    public void fuelConsumptionForOutOfBoundsAdvance() {
        Bulldozer bulldozer = new Bulldozer(singleLineSiteFor(ROCKY, PLAIN));
        try {
            bulldozer.advance(3);
            fail("Should have thrown OutOfBoundsException");
        } catch (OutOfBoundsException e) {
            // expected
        }
        assertThat(bulldozer.getConsumedFuel(), equalTo(3));
    }

    @Test
    public void paintDamageGoingThroughTree() {
        Bulldozer bulldozer = new Bulldozer(singleLineSiteFor(CLEARABLE_TREE, PLAIN));
        bulldozer.advance(2);
        assertThat(bulldozer.getPaintDamage(), equalTo(1));
    }

    @Test
    public void noPaintDamageIfStoppingOnTree() {
        Bulldozer bulldozer = new Bulldozer(singleLineSiteFor(CLEARABLE_TREE, PLAIN));
        bulldozer.advance(1);
        bulldozer.advance(1);
        assertThat(bulldozer.getPaintDamage(), equalTo(0));
    }

    @Test
    public void advanceOverProtectedTree() {
        Bulldozer bulldozer = new Bulldozer(singleLineSiteFor(PLAIN, PROTECTED_TREE));
        try {
            bulldozer.advance(2);
            fail("Should have thrown " + ProtectedTreeClearedException.class.getSimpleName());
        } catch (ProtectedTreeClearedException e) {
            // expected
        }
        assertThat(bulldozer.getConsumedFuel(), equalTo(3));
        assertThat(bulldozer.getPaintDamage(), equalTo(0));
    }

    @Test
    public void shouldStopOverProtectedTree() {
        Bulldozer bulldozer = new Bulldozer(singleLineSiteFor(PLAIN, PROTECTED_TREE, PLAIN));
        try {
            bulldozer.advance(3);
            fail("Should have thrown " + ProtectedTreeClearedException.class.getSimpleName());
        } catch (ProtectedTreeClearedException e) {
            // expected
        }
        assertThat(bulldozer.getPaintDamage(), equalTo(0));
        assertThat(bulldozer.getPosition(), equalTo(new Position(1, 0)));
    }

    private Site singleSquareSiteFor(SiteSquareState square) {
        return siteFor(new SiteSquareState[][]{{square}});
    }

    private Site singleLineSiteFor(SiteSquareState... squares) {
        SiteSquareState[][] state = new SiteSquareState[squares.length][1];
        for (int i = 0; i < squares.length; i++) {
            state[i][0] = squares[i];
        }
        return siteFor(state);
    }

    private Site siteFor(SiteSquareState[][] givenState) {
        return new Site(givenState);
    }
}