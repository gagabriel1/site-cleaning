package com.guernik.sitecleaning.domain;

import org.junit.Test;

import static com.guernik.sitecleaning.domain.Rotation.LEFT;
import static com.guernik.sitecleaning.domain.Rotation.RIGHT;
import static com.guernik.sitecleaning.domain.SiteSquareState.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class IntegrationTest {

    @Test
    public void exampleSimulation() {
        Site site = createSite();
        Session session = new Session(site);
        session.advance(4)
                .turn(RIGHT)
                .advance(4)
                .turn(LEFT)
                .advance(2)
                .advance(4)
                .turn(LEFT);
        Costs costs = session.getCosts();
        assertThat(costs.getFuelUsageCost().getUnits(), equalTo(19));
        assertThat(costs.getFuelUsageCost().getCost(), equalTo(19));
        assertThat(costs.getPaintDamageCost().getUnits(), equalTo(1));
        assertThat(costs.getPaintDamageCost().getCost(), equalTo(2));
        assertThat(costs.getUnclearedSquaresCost().getUnits(), equalTo(34));
        assertThat(costs.getUnclearedSquaresCost().getCost(), equalTo(102));
        assertThat(costs.getCommunicationOverheadCost().getUnits(), equalTo(7));
        assertThat(costs.getCommunicationOverheadCost().getCost(), equalTo(7));
    }

    private Site createSite() {
        SiteSquareState[][] state = {{ROCKY, ROCKY, ROCKY, PLAIN, PLAIN},
                {ROCKY, ROCKY, ROCKY, PLAIN, PLAIN},
                {ROCKY, ROCKY, ROCKY, PLAIN, CLEARABLE_TREE},
                {ROCKY, ROCKY, PLAIN, PLAIN, PLAIN},
                {ROCKY, PLAIN, PLAIN, PLAIN, PLAIN},
                {CLEARABLE_TREE, PLAIN, PLAIN, PLAIN, PLAIN},
                {PLAIN, PLAIN, PLAIN, PLAIN, PLAIN},
                {PLAIN, PLAIN, PROTECTED_TREE, PROTECTED_TREE, PLAIN},
                {PLAIN, PLAIN, PLAIN, PLAIN, PLAIN},
                {PLAIN, PLAIN, PLAIN, PLAIN, PLAIN}};
        return new Site(state);
    }
}
