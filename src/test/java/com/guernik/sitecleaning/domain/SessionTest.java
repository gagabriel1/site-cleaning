package com.guernik.sitecleaning.domain;

import org.junit.Before;
import org.junit.Test;

import static com.guernik.sitecleaning.domain.Rotation.RIGHT;
import static com.guernik.sitecleaning.domain.SiteSquareState.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class SessionTest {

    private Site site;

    @Before
    public void setUpSite() {
        SiteSquareState[][] givenState = {{CLEARABLE_TREE, ROCKY},
                {PLAIN, PROTECTED_TREE}};
        site = new Site(givenState);
    }

    @Test
    public void bulldozerShouldAdvance() {
        Session session = new Session(site);
        assertThat(session.getSiteStatus().unclearedSquares(), equalTo(3));

        session.advance(1);
        assertThat(session.getSiteStatus().unclearedSquares(), equalTo(2));
        assertThat(session.getCommandsAmount(), equalTo(1));
    }

    @Test
    public void bulldozerShouldTurn() {
        Session session = new Session(site);
        assertThat(session.getSiteStatus().unclearedSquares(), equalTo(3));

        session.advance(1);
        session.turn(RIGHT);
        session.advance(1);
        assertThat(session.getSiteStatus().unclearedSquares(), equalTo(1));
        assertThat(session.getCommandsAmount(), equalTo(3));
    }

    @Test(expected = SessionEndedException.class)
    public void advanceOnProtectedTreeShouldEndSession() {
        Session session = new Session(site);
        assertThat(session.getSiteStatus().unclearedSquares(), equalTo(3));

        try {
            session.advance(2);
            fail("Should have thrown " + ProtectedTreeClearedException.class.getSimpleName());
        } catch (ProtectedTreeClearedException e) {
            // expected
        }
        session.advance(1);
    }

    @Test
    public void protectedTreeDestroyedShouldBeCounted() {
        Session session = new Session(site);
        assertThat(session.getSiteStatus().unclearedSquares(), equalTo(3));
        try {
            session.advance(2);
            fail("Should have thrown " + ProtectedTreeClearedException.class.getSimpleName());
        } catch (ProtectedTreeClearedException e) {
            // expected
        }
        assertThat(session.getProtectedTreesDestroyed(), equalTo(1));
    }

    @Test(expected = SessionEndedException.class)
    public void quit() {
        Session session = new Session(site);
        session.quit();
    }

    @Test(expected = SessionEndedException.class)
    public void quitEndsTheSession() {
        Session session = new Session(site);
        try {
            session.quit();
            fail("Should have thrown " + SessionEndedException.class.getSimpleName());
        } catch (SessionEndedException e) {
            // expected
        }
        session.advance(1);
    }
}