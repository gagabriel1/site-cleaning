package com.guernik.sitecleaning.domain;

import com.guernik.sitecleaning.domain.Costs.CostItem;
import com.guernik.sitecleaning.domain.Costs.CostType;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collection;

import static com.guernik.sitecleaning.domain.Costs.CostType.*;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CostsTest {

    private static Matcher<Iterable<? super CostItem>> hasCostItemWithType(CostType type) {
        return hasItem(hasProperty("type", equalTo(type)));
    }

    @Test
    public void getPaintDamageCost() {
        BulldozerStatus bulldozerStatus = mock(BulldozerStatus.class);
        when(bulldozerStatus.getPaintDamage()).thenReturn(2);
        Session session = Mockito.mock(Session.class);
        when(session.getBulldozerStatus()).thenReturn(bulldozerStatus);
        Costs costs = new Costs(session);
        assertThat(costs.getPaintDamageCost().getUnits(), equalTo(2));
        assertThat(costs.getPaintDamageCost().getCost(), equalTo(4));
    }

    @Test
    public void getFuelUsageCost() {
        BulldozerStatus bulldozerStatus = mock(BulldozerStatus.class);
        when(bulldozerStatus.getConsumedFuelUnits()).thenReturn(2);
        Session session = Mockito.mock(Session.class);
        when(session.getBulldozerStatus()).thenReturn(bulldozerStatus);
        Costs costs = new Costs(session);
        assertThat(costs.getFuelUsageCost().getUnits(), equalTo(2));
        assertThat(costs.getFuelUsageCost().getCost(), equalTo(2));
    }

    @Test
    public void getUnclearedSquaresCost() {
        SiteStatus siteStatus = mock(SiteStatus.class);
        when(siteStatus.unclearedSquares()).thenReturn(13);
        Session session = Mockito.mock(Session.class);
        when(session.getSiteStatus()).thenReturn(siteStatus);
        Costs costs = new Costs(session);
        assertThat(costs.getUnclearedSquaresCost().getUnits(), equalTo(13));
        assertThat(costs.getUnclearedSquaresCost().getCost(), equalTo(39));
    }

    @Test
    public void getCommunicationOverheadCost() {
        Session session = Mockito.mock(Session.class);
        when(session.getCommandsAmount()).thenReturn(5);
        Costs costs = new Costs(session);
        assertThat(costs.getCommunicationOverheadCost().getUnits(), equalTo(5));
        assertThat(costs.getCommunicationOverheadCost().getCost(), equalTo(5));
    }

    @Test
    public void getProtectedTreeDestructionCost() {
        Session session = Mockito.mock(Session.class);
        when(session.getProtectedTreesDestroyed()).thenReturn(1);
        Costs costs = new Costs(session);
        assertThat(costs.getProtectedTreeDestructionCost().getUnits(), equalTo(1));
        assertThat(costs.getProtectedTreeDestructionCost().getCost(), equalTo(10));
    }

    @Test
    public void getTotalCost() {
        BulldozerStatus bulldozerStatus = mock(BulldozerStatus.class);
        when(bulldozerStatus.getConsumedFuelUnits()).thenReturn(1);
        when(bulldozerStatus.getPaintDamage()).thenReturn(10);
        SiteStatus siteStatus = mock(SiteStatus.class);
        when(siteStatus.unclearedSquares()).thenReturn(100);
        Session session = Mockito.mock(Session.class);
        when(session.getProtectedTreesDestroyed()).thenReturn(5);
        when(session.getCommandsAmount()).thenReturn(1000);
        when(session.getSiteStatus()).thenReturn(siteStatus);
        when(session.getBulldozerStatus()).thenReturn(bulldozerStatus);
        Costs costs = new Costs(session);
        assertThat(costs.getTotalCost(), equalTo(1 + 10 * 2 + 100 * 3 + 5 * 10 + 1000));
    }

    @Test
    public void getAllCost() {
        BulldozerStatus bulldozerStatus = mock(BulldozerStatus.class);
        SiteStatus siteStatus = mock(SiteStatus.class);
        Session session = Mockito.mock(Session.class);
        when(session.getSiteStatus()).thenReturn(siteStatus);
        when(session.getBulldozerStatus()).thenReturn(bulldozerStatus);
        Costs costs = new Costs(session);
        Collection<CostItem> allCosts = costs.getAllCosts();
        assertThat(allCosts, hasSize(5));
        assertThat(allCosts, hasCostItemWithType(PAINT_DAMAGE));
        assertThat(allCosts, hasCostItemWithType(FUEL_USAGE));
        assertThat(allCosts, hasCostItemWithType(COMMUNICATION_OVERHEAD));
        assertThat(allCosts, hasCostItemWithType(PROTECTED_TREE_DESTRUCTION));
        assertThat(allCosts, hasCostItemWithType(UNCLEARED_SQUARE));
    }
}