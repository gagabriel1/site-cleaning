package com.guernik.sitecleaning;


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class AppTest {

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    @Test
    public void appRunWithQuitCommand() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream(100_000);
        InputStream input = new ByteArrayInputStream("a 1\nr l q\n".getBytes());
        App app = new App(fileWithContent("to\nTr"), input, new PrintStream(out));
        app.run();
        String output = out.toString();
        assertThat(output, containsString("Welcome to the Aconex site clearing simulator."));
        assertThat(output, containsString("t o\nT r"));
        assertThat(output, containsString("The simulation has ended at your request."));
        assertThat(output, containsString("advance 1, right, left, quit"));
        assertThat(output, containsString("Thank you for using the Aconex site clearing simulator."));
        assertThat(output, containsString("paint damage to bulldozer                        0         0"));
        assertThat(output, containsString("fuel usage                                       2         2"));
        assertThat(output, containsString("uncleared squares                                2         6"));
        assertThat(output, containsString("communication overhead                           3         3"));
        assertThat(output, containsString("destruction of protected tree                    0         0"));
        assertThat(output, containsString("Total                                                     11"));
    }

    @Test
    public void appRunWithProtectedTreeDestroyed() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream(100_000);
        InputStream input = new ByteArrayInputStream("a 1\nr a 1 q\n".getBytes());
        App app = new App(fileWithContent("to\nTr"), input, new PrintStream(out));
        app.run();
        String output = out.toString();
        assertThat(output, containsString("The simulation has ended as you destroyed a protected tree."));
        assertThat(output, containsString("advance 1, right, advance 1\n"));
    }

    @Test
    public void appRunGoingOutOfSiteBounds() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream(100_000);
        InputStream input = new ByteArrayInputStream("a 3 q\n".getBytes());
        App app = new App(fileWithContent("to\nTr"), input, new PrintStream(out));
        app.run();
        String output = out.toString();
        assertThat(output, containsString("The simulation has ended as you tried to go out of the bounds of the site."));
        assertThat(output, containsString("advance 3\n"));
    }

    private File fileWithContent(final String siteString) throws IOException {
        File file = testFolder.newFile("sample.txt");
        final FileWriter writer = new FileWriter(file);
        writer.write(siteString);
        writer.close();
        return file;
    }
}