package com.guernik.sitecleaning.ports;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class FileSiteFactoryTest {

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    @Test
    public void createSite() throws IOException {
        final String siteString = "tr\nTo";
        File file = fileWithContent(siteString);
        TextSiteFactory textFactory = mock(TextSiteFactory.class);
        new FileSiteFactory(textFactory).createSite(file);
        verify(textFactory).createSite(siteString);
    }

    private File fileWithContent(final String siteString) throws IOException {
        File file = testFolder.newFile("test.txt");
        final FileWriter writer = new FileWriter(file);
        writer.write(siteString);
        writer.close();
        return file;
    }
}