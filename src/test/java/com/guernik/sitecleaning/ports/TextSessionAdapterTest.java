package com.guernik.sitecleaning.ports;

import com.guernik.sitecleaning.domain.Session;
import com.guernik.sitecleaning.domain.SessionEndedException;
import com.guernik.sitecleaning.domain.Site;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static com.guernik.sitecleaning.domain.Rotation.LEFT;
import static com.guernik.sitecleaning.domain.Rotation.RIGHT;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(Enclosed.class)
public class TextSessionAdapterTest {

    public static abstract class CommonSetup {

        protected Session session;
        protected TextSessionAdapter adapter;

        @Before
        public void setUp() {
            session = mock(Session.class);
            adapter = new TextSessionAdapter(session);
        }
    }

    public static class RegularTests extends CommonSetup {

        @Test
        public void siteMapFromNorthWestCornerHorizontally() {
            // this test involves more behaviour than necessary (we could use mocks instead of a real
            // Session) but the benefit is that it tests that the actual printing is consistent with
            // the initial Site creation from a String definition of the squares in the site
            Site site = new TextSiteFactory().createSite("tT\nor");
            TextSessionAdapter adapter = new TextSessionAdapter(new Session(site));
            assertThat(adapter.siteMapFromNorthWestCornerHorizontally(), equalTo("t T\no r"));
        }

        @Test
        public void commandList() {
            // it is not ideal to use actual instances as this test involves too much behavior and thus
            // it can be brittle, but it is complex to create commands outside of Session as the
            // constructors have private access
            Site site = new TextSiteFactory().createSite("t");
            Session session = new Session(site);
            try {
                session.advance(1).turn(RIGHT).turn(LEFT).quit();
            } catch (SessionEndedException e) {
                // expected
            }
            TextSessionAdapter adapter = new TextSessionAdapter(session);
            assertThat(adapter.commandList(), equalTo("advance 1, right, left, quit"));
        }

        @Test
        public void advanceCommand() {
            adapter.handleCommands("a 1");
            verify(session).advance(1);

            adapter.handleCommands(" a 2 ");
            verify(session).advance(2);

            adapter.handleCommands("advance 1234");
            verify(session).advance(1234);
        }

        @Test
        public void turnRightCommand() {
            adapter.handleCommands("r");
            verify(session).turn(RIGHT);

            reset(session);
            adapter.handleCommands("right");
            verify(session).turn(RIGHT);

            reset(session);
            adapter.handleCommands(" r ");
            verify(session).turn(RIGHT);
        }

        @Test
        public void turnLeftCommand() {
            adapter.handleCommands("l");
            verify(session).turn(LEFT);

            reset(session);
            adapter.handleCommands("left");
            verify(session).turn(LEFT);

            reset(session);
            adapter.handleCommands(" l ");
            verify(session).turn(LEFT);
        }

        @Test
        public void quitCommand() {
            adapter.handleCommands("q");
            verify(session).quit();

            reset(session);
            adapter.handleCommands("quit");
            verify(session).quit();

            reset(session);
            adapter.handleCommands(" q ");
            verify(session).quit();
        }

        @Test
        public void handleMultipleCommands() {
            adapter.handleCommands("a 1 l r q");
            verify(session).advance(1);
            verify(session).turn(LEFT);
            verify(session).turn(RIGHT);
            verify(session).quit();
        }

        @Test
        public void handleMultipleCommandsSeparatedByMultipleSpaces() {
            adapter.handleCommands(" a   1   l    ");
            verify(session).advance(1);
            verify(session).turn(LEFT);
        }

        @Test
        public void noCommandIsExecutedIfThereIsAParseError() {
            try {
                adapter.handleCommands("a 1 l r a l");
                fail("Should have thrown " + IllegalArgumentException.class.getSimpleName());
            } catch (IllegalArgumentException e) {
                // expected
            }
            verify(session, never()).advance(anyInt());
            verify(session, never()).turn(LEFT);
            verify(session, never()).turn(RIGHT);
            verify(session, never()).quit();
        }

        @Test
        public void noCommandIsExecutedAfterQuit() {
            doThrow(new SessionEndedException()).when(session).quit();
            try {
                adapter.handleCommands("a 1 q l r a 2");
                fail("Should have thrown " + SessionEndedException.class.getSimpleName());
            } catch (SessionEndedException e) {
                // expected
            }
            verify(session).advance(1);
            verify(session).quit();
            verify(session, never()).turn(LEFT);
            verify(session, never()).turn(RIGHT);
        }
    }

    @RunWith(Parameterized.class)
    public static class InvalidCommandsTests extends CommonSetup {

        @Parameters(name = "\"{0}\" is invalid command")
        public static Collection<? extends Object> data() {
            return Arrays.asList(
                    "a",
                    "advance",
                    "advancee 1",
                    "aadvance 1",
                    "aa 1",
                    "a a 1",
                    "a 1 1",
                    "a a",
                    "righ",
                    "rightt",
                    "rright",
                    "lef",
                    "leftt",
                    "lleft",
                    "qui",
                    "quitt",
                    "qquit");
        }

        @Parameter
        public String command;

        @Test(expected = IllegalArgumentException.class)
        public void invalidCommand() {
            adapter.handleCommands(command);
        }
    }
}