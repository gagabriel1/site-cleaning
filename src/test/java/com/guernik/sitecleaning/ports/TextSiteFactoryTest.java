package com.guernik.sitecleaning.ports;

import com.guernik.sitecleaning.domain.Site;
import com.guernik.sitecleaning.domain.SiteSquareState;
import org.junit.Test;

import static com.guernik.sitecleaning.domain.SiteSquareState.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class TextSiteFactoryTest {

    @Test
    public void createSite() {
        String siteString = "tr\nTo";
        Site site = new TextSiteFactory().createSite(siteString);
        assertThat(site.getState(), equalTo(new SiteSquareState[][]{{PROTECTED_TREE, CLEARABLE_TREE},
                {PLAIN, ROCKY}}));
    }
}