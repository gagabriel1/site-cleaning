package com.guernik.sitecleaning;

import com.guernik.sitecleaning.domain.*;
import com.guernik.sitecleaning.ports.FileSiteFactory;
import com.guernik.sitecleaning.ports.TextSessionAdapter;
import com.guernik.sitecleaning.ports.TextSiteFactory;

import java.io.File;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Main class for the application.
 * <p>
 * Starts a session that accepts commands from the command line and provides output in the same
 * way. The {@link Site} map must be loaded from a text file given as input to the program.
 */
public class App {

    private final Site site;
    private final Scanner input;
    private final PrintWriter output;
    private final TextSessionAdapter textSession;

    public App(File file, InputStream in, PrintStream out) {
        this.site = new FileSiteFactory(new TextSiteFactory()).createSite(file);
        this.textSession = new TextSessionAdapter(new Session(site));
        this.input = new Scanner(in);
        this.output = new PrintWriter(out);
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Only a single parameter is required, the file path to the site "
                    + "definition");
            return;
        }
        String filePath = args[0];
        File file = new File(filePath);
        if (!file.exists()) {
            System.out.println("There is no file with path " + file.getAbsolutePath());
            return;
        }
        new App(file, System.in, System.out).run();
    }

    void run() {
        printWelcome();
        String endReason;
        try {
            do {
                String command = requestCommand();
                try {
                    textSession.handleCommands(command);
                } catch (IllegalArgumentException e) {
                    output.printf("Invalid command: '%s'. Please try again.\n", command);
                }
            } while (true);
        } catch (OutOfBoundsException e) {
            endReason = "The simulation has ended as you tried to go out of the bounds of the site.";
        } catch (ProtectedTreeClearedException e) {
            endReason = "The simulation has ended as you destroyed a protected tree.";
        } catch (SessionEndedException e) {
            endReason = "The simulation has ended at your request.";
        }
        printEnd(endReason);
    }

    private String requestCommand() {
        output.print("(l)eft, (r)ight, (a)dvance <n>, (q)uit: ");
        output.flush();
        return this.input.nextLine();
    }

    private void printWelcome() {
        output.println("Welcome to the Aconex site clearing simulator. This is a map of the site:\n");
        output.println(textSession.siteMapFromNorthWestCornerHorizontally());
        output.println("");
        output.println("The bulldozer is currently located at the Northern edge of the site, "
                + "immediately to the West of the site, and facing East.\n");
        output.flush();
    }

    private void printEnd(final String endReason) {
        output.printf("\n%s These are the commands you issued:\n\n", endReason);
        output.println(textSession.commandList());
        output.println("");
        output.println("The costs for this land clearing operation were:\n");
        printCosts();
        output.println("\nThank you for using the Aconex site clearing simulator.");
        output.flush();
    }

    private void printCosts() {
        String format = "%-35s%15s%10s\n";
        output.printf(format, "Item", "Quantity", "Cost");
        Costs costs = textSession.getCosts();
        for (Costs.CostItem item : costs.getAllCosts()) {
            output.printf(format, costNameFrom(item.getType()), item.getUnits(), item.getCost());
        }
        output.printf(format, "----", "", "");
        output.printf(format, "Total", "", costs.getTotalCost());
    }

    private String costNameFrom(final Costs.CostType type) {
        switch (type) {
            case FUEL_USAGE:
                return "fuel usage";
            case COMMUNICATION_OVERHEAD:
                return "communication overhead";
            case PAINT_DAMAGE:
                return "paint damage to bulldozer";
            case PROTECTED_TREE_DESTRUCTION:
                return "destruction of protected tree";
            case UNCLEARED_SQUARE:
                return "uncleared squares";
            default:
                return "unknown cost type";
        }
    }
}
