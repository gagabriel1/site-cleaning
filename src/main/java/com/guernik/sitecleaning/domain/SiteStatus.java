package com.guernik.sitecleaning.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * View of a {@link Site}.
 * <p>
 * This provides a way to review the current state without access to modify it.
 * Changes to the underlying {@link Site} will be reflected in instances of this class.
 */
public class SiteStatus {

    private final Site site;

    public SiteStatus(final Site site) {
        this.site = site;
    }

    public int unclearedSquares() {
        return site.unclearedSquares();
    }

    /**
     * Retrieves the squares from the site, starting from the North-West corner.
     *
     * @return a list of lists of the site squares. Each inner list corresponds to a row (West to
     * East direction) ordered West to East. The enclosing list is ordered from North to South.
     */
    public List<List<SiteSquareState>> fromNorthWestCorner() {
        SiteSquareState[][] state = site.getState();
        List<List<SiteSquareState>> view = new ArrayList<>(site.getNorthSouthLength());
        for (int ns = 0; ns < site.getNorthSouthLength(); ns++) {
            List<SiteSquareState> westEastLine = new ArrayList<>(state.length);
            for (SiteSquareState[] line : state) {
                westEastLine.add(line[site.getNorthSouthLength() - ns - 1]);
            }
            view.add(westEastLine);
        }
        return view;
    }
}
