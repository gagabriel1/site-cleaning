package com.guernik.sitecleaning.domain;

/**
 * The possible states of the squares in a {@link Site}.
 */
public enum SiteSquareState {
    PLAIN, ROCKY, CLEARABLE_TREE, PROTECTED_TREE, CLEARED
}
