package com.guernik.sitecleaning.domain;

/**
 * Represents the possible turns the bulldozer can execute.
 */
public enum Rotation {
    RIGHT, LEFT
}
