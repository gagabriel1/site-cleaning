package com.guernik.sitecleaning.domain;

import java.util.LinkedList;
import java.util.List;

/**
 * A training session.
 * <p>
 * Instances of this class are not thread safe.
 */
public class Session {

    private final Site site;
    private final Bulldozer bulldozer;
    private final List<Command> commands = new LinkedList<>();
    private int commandsAmount = 0;
    private int protectedTreesDestroyed = 0;
    private boolean ended = false;

    public Session(Site site) {
        this.site = site;
        this.bulldozer = new Bulldozer(site);
    }

    /**
     * Advances the {@link Bulldozer} used in this session.
     * If any exception listed below is thrown, the session will be ended.
     *
     * @param distance the squares to advance
     * @return the same {@link Session}
     * @throws OutOfBoundsException          if trying to move outside of the site
     * @throws ProtectedTreeClearedException if a protected tree is cleared
     * @throws SessionEndedException         if the session was already ended
     */
    public Session advance(int distance) {
        commands.add(new AdvanceCommand(distance));
        handleCommand(() -> bulldozer.advance(distance));
        return this;
    }

    /**
     * Turns the {@link Bulldozer} used in this session.
     *
     * @param rotation the side to turn to
     * @return the same {@link Session}
     * @throws SessionEndedException if the session was already ended
     */
    public Session turn(Rotation rotation) {
        commands.add(new TurnCommand(rotation));
        handleCommand(() -> bulldozer.turn(rotation));
        return this;
    }

    /**
     * Ends the session.
     *
     * @throws SessionEndedException every time it is called
     */
    public void quit() {
        commands.add(new QuitCommand());
        ended = true;
        checkIsEnded();
    }

    private void handleCommand(Runnable action) {
        checkIsEnded();
        commandsAmount++;
        try {
            action.run();
        } catch (ProtectedTreeClearedException e) {
            ended = true;
            protectedTreesDestroyed++;
            throw e;
        }
    }

    private void checkIsEnded() {
        if (ended) {
            throw new SessionEndedException();
        }
    }

    public Costs getCosts() {
        return new Costs(this);
    }

    /**
     * @return an unmodifiable view of the {@link Site} used in this {@link Session}
     */
    public SiteStatus getSiteStatus() {
        return new SiteStatus(site);
    }

    /**
     * @return an unmodifiable view of the {@link Bulldozer} used in this {@link Session}
     */
    public BulldozerStatus getBulldozerStatus() {
        return new BulldozerStatus(bulldozer);
    }

    /**
     * @return the amount of commands forwarded to the {@link Bulldozer}. It does not include a
     * possible last quit command not any command submitted after the {@link Session} has ended, as
     * none of these commands are sent to the {@link Bulldozer}.
     */
    public int getCommandsAmount() {
        return commandsAmount;
    }

    public int getProtectedTreesDestroyed() {
        return protectedTreesDestroyed;
    }

    /**
     * @return a list of all the commands executed, in the order they were received and executed.
     * It includes a possible last quit command or a command that causes the {@link Session} to end
     * due to an error condition, but not any command after that.
     */
    public List<Command> getCommands() {
        return commands;
    }

    /**
     * Represents a possible command executed by a {@link Session}.
     */
    public interface Command {
    }

    public static final class AdvanceCommand implements Command {

        private final int distance;

        private AdvanceCommand(int distance) {
            this.distance = distance;
        }

        public int getDistance() {
            return distance;
        }
    }

    public static final class TurnCommand implements Command {

        private final Rotation rotation;

        private TurnCommand(Rotation rotation) {
            this.rotation = rotation;
        }

        public Rotation getRotation() {
            return rotation;
        }
    }

    public static final class QuitCommand implements Command {

        private QuitCommand() {
        }
    }
}
