package com.guernik.sitecleaning.domain;

/**
 * Exceptions that means that a protected tree was cleared.
 */
public class ProtectedTreeClearedException extends RuntimeException {

    public ProtectedTreeClearedException(String message) {
        super(message);
    }
}
