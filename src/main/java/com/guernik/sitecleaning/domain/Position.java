package com.guernik.sitecleaning.domain;

import java.util.Objects;

/**
 * Represents a possible position in a site. But it does not belong to any particular site.
 * Instances of this class are immutable.
 */
public class Position {

    private final int x;
    private final int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Calculates the position that results from moving this one by the given movement.
     * This position remains unchanged.
     *
     * @return a new {@link Position}
     */
    public Position plus(Movement movement) {
        return new Position(x + movement.getX(), y + movement.getY());
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Position position = (Position) o;
        return x == position.x &&
                y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Position{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
