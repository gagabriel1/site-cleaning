package com.guernik.sitecleaning.domain;

import static com.guernik.sitecleaning.domain.Direction.EAST;
import static com.guernik.sitecleaning.domain.SiteSquareState.CLEARABLE_TREE;
import static com.guernik.sitecleaning.domain.SiteSquareState.PROTECTED_TREE;
import static com.guernik.sitecleaning.validation.Checks.checkNotNull;

public class Bulldozer {

    private final Site site;
    private Position position;
    private Direction direction;
    private int consumedFuel;
    private int paintDamage;

    public Bulldozer(Site site) {
        this.site = checkNotNull(site, "Site to work must not be null");
        this.position = new Position(-1, site.getNorthSouthLength() - 1);
        this.direction = EAST;
    }

    public Position getPosition() {
        return position;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getConsumedFuel() {
        return consumedFuel;
    }

    public int getPaintDamage() {
        return paintDamage;
    }

    public Bulldozer turn(Rotation rotation) {
        direction = direction.rotate(rotation);
        return this;
    }

    /**
     * Advances the bulldozer.
     *
     * @param distance the squares to advance
     * @return the same {@link Bulldozer}
     * @throws OutOfBoundsException          if trying to move outside of the site
     * @throws ProtectedTreeClearedException if a protected tree is cleared
     */
    public Bulldozer advance(int distance) {
        while (distance > 0) {
            Position newPosition = position.plus(direction.getMovement(1));
            position = newPosition;
            --distance;
            SiteSquareState previousSquareState = site.clear(newPosition);
            boolean isPassingThrough = distance > 0;
            updateConsumedFuel(previousSquareState);
            updatePaintDamage(previousSquareState, isPassingThrough);
            checkProtectedTree(previousSquareState, position);
        }
        return this;
    }

    private void checkProtectedTree(SiteSquareState square, Position position) {
        if (square == PROTECTED_TREE) {
            throw new ProtectedTreeClearedException("Cleared protected tree at " + position);
        }
    }

    private void updatePaintDamage(SiteSquareState previousSquareState, boolean isPassingThrough) {
        if (isPassingThrough && CLEARABLE_TREE == previousSquareState) {
            paintDamage++;
        }
    }

    private void updateConsumedFuel(SiteSquareState previousSquareState) {
        int newlyConsumedFuel = 0;
        switch (previousSquareState) {
            case CLEARED:
            case PLAIN:
                newlyConsumedFuel = 1;
                break;
            case ROCKY:
            case CLEARABLE_TREE:
            case PROTECTED_TREE:
                newlyConsumedFuel = 2;
                break;
        }
        this.consumedFuel += newlyConsumedFuel;
    }
}
