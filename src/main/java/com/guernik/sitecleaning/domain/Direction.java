package com.guernik.sitecleaning.domain;

import java.util.Arrays;
import java.util.List;

/**
 * Represents the possible directions a bulldozer can be facing.
 */
public enum Direction {
    NORTH(0, 1), SOUTH(0, -1), WEST(-1, 0), EAST(1, 0);

    private static final List<Direction> LEFT_ROTATION_ORDER =
            Arrays.asList(NORTH, WEST, SOUTH, EAST);

    private final Movement movement;

    Direction(int x, int y) {
        this.movement = new Movement(x, y);
    }

    /**
     * Returns the movement that results from moving distance units in this direction.
     */
    public Movement getMovement(int distance) {
        return new Movement(movement.getX() * distance, movement.getY() * distance);
    }

    /**
     * Calculates the direction resulting from applying the given rotation
     * to this direction.
     */
    public Direction rotate(Rotation rotation) {
        int leftRotationShift;
        switch (rotation) {
            case LEFT:
                leftRotationShift = 1;
                break;
            case RIGHT:
                leftRotationShift = -1;
                break;
            default:
                throw new IllegalArgumentException("Not a valid rotation");
        }
        int directionsCount = LEFT_ROTATION_ORDER.size();
        int currentDirectionIndex = LEFT_ROTATION_ORDER.indexOf(this);
        int newDirectionIndex =
                (currentDirectionIndex + leftRotationShift + directionsCount) % directionsCount;
        return LEFT_ROTATION_ORDER.get(newDirectionIndex);
    }
}
