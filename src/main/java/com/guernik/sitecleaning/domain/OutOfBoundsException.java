package com.guernik.sitecleaning.domain;

/**
 * Exception meaning that a position outside of a {@link Site} was tried to be accessed.
 */
public class OutOfBoundsException extends RuntimeException {

    public OutOfBoundsException(final String message) {
        super(message);
    }
}
