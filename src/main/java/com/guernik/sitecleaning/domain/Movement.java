package com.guernik.sitecleaning.domain;

/**
 * Represent a possible single move of the bulldozer.
 * It is basically a 2-dimensional vector.
 */
public class Movement {

    private final int x;
    private final int y;

    public Movement(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
