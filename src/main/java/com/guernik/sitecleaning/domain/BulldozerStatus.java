package com.guernik.sitecleaning.domain;

/**
 * A view of a {@link Bulldozer}.
 * Instance of this class cannot be updated but are not immutable, as they track changes to the
 * underlying {@link Bulldozer}.
 */
public class BulldozerStatus {

    private final Bulldozer bulldozer;

    BulldozerStatus(Bulldozer bulldozer) {
        this.bulldozer = bulldozer;
    }

    public int getConsumedFuelUnits() {
        return bulldozer.getConsumedFuel();
    }

    public int getPaintDamage() {
        return bulldozer.getPaintDamage();
    }

}
