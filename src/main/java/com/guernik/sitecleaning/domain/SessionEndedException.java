package com.guernik.sitecleaning.domain;

/**
 * Exception that means that the session is ended before or after executing a command.
 */
public class SessionEndedException extends RuntimeException {

    public SessionEndedException() {
        super();
    }
}
