package com.guernik.sitecleaning.domain;

import java.util.Arrays;
import java.util.Collection;

import static com.guernik.sitecleaning.domain.Costs.CostType.*;

/**
 * A container for all the costs associated to a {@link Session}.
 */
public class Costs {

    private final Session session;

    Costs(Session session) {
        this.session = session;
    }

    public CostItem getPaintDamageCost() {
        return new CostItem(session.getBulldozerStatus().getPaintDamage(), PAINT_DAMAGE);
    }

    public CostItem getFuelUsageCost() {
        return new CostItem(session.getBulldozerStatus().getConsumedFuelUnits(), FUEL_USAGE);
    }

    public CostItem getUnclearedSquaresCost() {
        SiteStatus siteStatus = session.getSiteStatus();
        return new CostItem(siteStatus.unclearedSquares(), UNCLEARED_SQUARE);
    }

    public CostItem getCommunicationOverheadCost() {
        return new CostItem(session.getCommandsAmount(), COMMUNICATION_OVERHEAD);
    }

    public CostItem getProtectedTreeDestructionCost() {
        return new CostItem(session.getProtectedTreesDestroyed(), PROTECTED_TREE_DESTRUCTION);
    }

    public Collection<CostItem> getAllCosts() {
        return Arrays.asList(getPaintDamageCost(),
                getFuelUsageCost(),
                getUnclearedSquaresCost(),
                getCommunicationOverheadCost(),
                getProtectedTreeDestructionCost());
    }

    public int getTotalCost() {
        return getAllCosts().stream().mapToInt(CostItem::getCost).sum();
    }

    public enum CostType {
        PAINT_DAMAGE(2),
        FUEL_USAGE(1),
        UNCLEARED_SQUARE(3),
        COMMUNICATION_OVERHEAD(1),
        PROTECTED_TREE_DESTRUCTION(10);

        public int costPerUnit;

        CostType(final int costPerUnit2) {
            costPerUnit = costPerUnit2;
        }
    }

    public static class CostItem {

        private final int units;
        private final CostType costType;

        private CostItem(int units, CostType type) {
            this.units = units;
            this.costType = type;
        }

        public int getUnits() {
            return units;
        }

        public int getCost() {
            return units * costType.costPerUnit;
        }

        public CostType getType() {
            return costType;
        }
    }
}
