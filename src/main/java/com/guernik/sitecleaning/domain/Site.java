package com.guernik.sitecleaning.domain;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.guernik.sitecleaning.domain.SiteSquareState.CLEARED;
import static com.guernik.sitecleaning.domain.SiteSquareState.PROTECTED_TREE;

/**
 * A site for a {@link Bulldozer} to work on.
 * <p>
 * Instances of this class are not thread safe.
 */
public class Site {

    private final SiteSquareState[][] state;

    /**
     * Creates a site from a map of the squares.
     *
     * @param initialState a map of the squares for the sie. The first index grows in West to East
     *                     direction, and the second one in South to North direction. The {@code
     *                     initialState} is copied so any changes after construcion of the
     *                     {@link Site} will have no effect on the state of the {@link Site}
     * @throws IllegalArgumentException if the map is null, has any null element, or inner arrays
     *                                  are not all of the same length.
     */
    public Site(SiteSquareState[][] initialState) {
        this.state = initState(initialState);
    }

    private SiteSquareState[][] initState(SiteSquareState[][] givenState) {
        checkIsNotEmpty(givenState);
        checkIsNotJagged(givenState);
        checkHasNoNulls(givenState);
        return copyState(givenState);
    }

    private static void checkHasNoNulls(SiteSquareState[][] givenState) {
        forEachSquare(givenState,
                Objects::isNull,
                square -> {
                    throw new IllegalArgumentException("Site must not contain null elements");
                }
        );
    }

    private static SiteSquareState[][] copyState(SiteSquareState[][] givenState) {
        SiteSquareState[][] initialState = new SiteSquareState[givenState.length][givenState[0].length];
        for (int i = 0; i < givenState.length; i++) {
            System.arraycopy(givenState[i], 0, initialState[i], 0, givenState[i].length);
        }
        return initialState;
    }

    private static void checkIsNotEmpty(SiteSquareState[][] state) {
        if (state == null) {
            throw new IllegalArgumentException("Site must not be null");
        }
        for (SiteSquareState[] line : state) {
            if (line == null) {
                throw new IllegalArgumentException("Site must not contain null rows");
            }
        }
        if (state.length == 0 || state[0].length == 0) {
            throw new IllegalArgumentException("Site must not be empty");
        }
    }

    private static void checkIsNotJagged(final SiteSquareState[][] state) {
        if (state.length == 0 || state.length == 1) {
            return;
        }
        int expectedLength = state[0].length;
        for (SiteSquareState[] line : state) {
            if (line.length != expectedLength) {
                throw new IllegalArgumentException("Construction site must be rectangular");
            }
        }
    }

    /**
     * @return a view of the state of the squares on the {@link Site}. Changes to the returned
     * instance will have no effect in the state of the {@link Site}.
     */
    public SiteSquareState[][] getState() {
        return copyState(state);
    }

    /**
     * Clears the square in the given {@code position}. If the {@code position} is valid, the
     * square will always be cleared after invocations of this method.
     *
     * @param position the {@code position} to clear
     * @return the previous state of the square
     * @throws OutOfBoundsException if the {@code position} is out of the site
     */
    public SiteSquareState clear(Position position) {
        try {
            SiteSquareState previousState = state[position.getX()][position.getY()];
            state[position.getX()][position.getY()] = CLEARED;
            return previousState;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new OutOfBoundsException(position + " is out of the site bounds");
        }
    }

    public int getNorthSouthLength() {
        return state[0].length;
    }

    /**
     * @return the amount of squares for the given type at the moment of invocation
     */
    public int amountOfSquaresFor(SiteSquareState squareType) {
        // this is used just as a counter, not due to concurrency considerations
        AtomicInteger amount = new AtomicInteger(0);
        forEachSquare(state,
                squareType::equals,
                square -> amount.incrementAndGet()
        );
        return amount.get();
    }

    /**
     * @return the amount of squares that could potentially be cleared that are not cleared. It does
     * not include squares with protected trees.
     */
    public int unclearedSquares() {
        int total = state.length * state[0].length;
        return total - amountOfSquaresFor(CLEARED) - amountOfSquaresFor(PROTECTED_TREE);
    }

    /**
     * Executes the {@code action} on each square of {@code state} if {@code condition} is true for
     * the square.
     *
     * @param state
     * @param condition
     * @param action
     */
    private static void forEachSquare(SiteSquareState[][] state,
                                      Function<SiteSquareState, Boolean> condition,
                                      Consumer<SiteSquareState> action) {
        for (SiteSquareState[] line : state) {
            for (SiteSquareState square : line) {
                if (condition.apply(square)) {
                    action.accept(square);
                }
            }
        }
    }
}
