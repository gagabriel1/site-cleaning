package com.guernik.sitecleaning.validation;

public class Checks {

    private Checks() {
    }

    /**
     * Ensures the truth of an expression involving one or more parameters to the calling method.
     *
     * @param expression   a boolean expression
     * @param errorMessage the exception message to use if the check fails
     * @throws IllegalArgumentException if {@code expression} is false
     */
    public static void checkArgument(boolean expression, String errorMessage) {
        if (!expression) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    /**
     * Ensures that the given object is not null.
     *
     * @param object       the object to check
     * @param errorMessage the exception message to use if the check fails
     * @return the given {@code object}
     * @throws IllegalArgumentException if {@code object} is null
     */
    public static <T> T checkNotNull(T object, String errorMessage) {
        checkArgument(object != null, errorMessage);
        return object;
    }
}
