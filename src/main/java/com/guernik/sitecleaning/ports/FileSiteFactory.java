package com.guernik.sitecleaning.ports;

import com.guernik.sitecleaning.domain.Site;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;

import static java.nio.file.Files.readAllLines;

/**
 * Factory class for {@link Site} based on the contents of a file.
 */
public class FileSiteFactory {

    private final TextSiteFactory textFactory;

    public FileSiteFactory(TextSiteFactory textSiteFactory) {
        this.textFactory = textSiteFactory;
    }

    /**
     * Builds a site based on the contents of {@param file}.
     * The file must contain a text representation of the site in a format compatible with
     * {@link TextSiteFactory}.
     *
     * @param file the file contain the text representation of the site
     * @return a {@link Site} ready to be used
     * @throws UncheckedIOException if there is any issue reading the file
     */
    public Site createSite(File file) {
        try {
            String siteString = String.join("\n", readAllLines(file.toPath()));
            return textFactory.createSite(siteString);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
