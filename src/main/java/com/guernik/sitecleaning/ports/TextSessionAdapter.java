package com.guernik.sitecleaning.ports;

import com.guernik.sitecleaning.domain.Costs;
import com.guernik.sitecleaning.domain.Session;
import com.guernik.sitecleaning.domain.Session.AdvanceCommand;
import com.guernik.sitecleaning.domain.Session.Command;
import com.guernik.sitecleaning.domain.Session.QuitCommand;
import com.guernik.sitecleaning.domain.Session.TurnCommand;
import com.guernik.sitecleaning.domain.SiteSquareState;

import java.util.*;
import java.util.stream.Collectors;

import static com.guernik.sitecleaning.domain.Rotation.LEFT;
import static com.guernik.sitecleaning.domain.Rotation.RIGHT;
import static java.util.stream.Collectors.joining;

public class TextSessionAdapter {

    private final Session session;

    public TextSessionAdapter(Session session) {
        this.session = session;
    }

    private static String toString(Command command) {
        if (command instanceof AdvanceCommand) {
            return "advance " + ((AdvanceCommand) command).getDistance();
        } else if (command instanceof TurnCommand) {
            return ((TurnCommand) command).getRotation().name().toLowerCase();
        } else if (command instanceof QuitCommand) {
            return "quit";
        } else {
            return "???";
        }
    }

    private static String toString(List<SiteSquareState> squareStates) {
        return squareStates.stream().map(TextSessionAdapter::toString).collect(joining(" "));
    }

    private static String toString(SiteSquareState square) {
        switch (square) {
            case CLEARABLE_TREE:
                return "t";
            case PLAIN:
                return "o";
            case PROTECTED_TREE:
                return "T";
            case ROCKY:
                return "r";
            case CLEARED:
                return "-";
            default:
                return "?";
        }
    }

    /**
     * Parses and executes a sequence of commands.
     * Valid commands are:
     * <ul>
     * <li>advance n (where n is a positive integer): to advance n squares, can be also only 'a n'
     * </li>
     * <li>right: turn right, can be also only 'r'</li>
     * <li>left: turn left, can be also only 'l'</li>
     * <li>quit: ends the simulation, can be also only 'q'</li>
     * </ul>
     *
     * @param commandsString a valid sequence of commands separated by spaces (1 or more)
     * @throws IllegalArgumentException if some command string is not recognized
     */
    public void handleCommands(String commandsString) {
        commandsString = commandsString.trim();
        Iterator<String> commands = toIterator(commandsString);
        List<Runnable> actualCommands = parseAllCommands(commands);
        actualCommands.forEach(Runnable::run);
    }

    private List<Runnable> parseAllCommands(final Iterator<String> commands) {
        List<Runnable> result = new LinkedList<>();
        while (commands.hasNext()) {
            String command = commands.next();
            result.add(parseCommand(commands, command));
        }
        return result;
    }

    private Runnable parseCommand(final Iterator<String> commands, final String command) {
        // a recursive function could be used but this is simple enough
        if ("l".equals(command) || "left".equals(command)) {
            return () -> session.turn(LEFT);
        } else if ("r".equals(command) || "right".equals(command)) {
            return () -> session.turn(RIGHT);
        } else if ("a".equals(command) || "advance".equals(command)) {
            try {
                int squaresToAdvance = Integer.parseInt(commands.next());
                return () -> session.advance(squaresToAdvance);
            } catch (NumberFormatException | NoSuchElementException e) {
                throw new IllegalArgumentException("Missing valid distance for advance command");
            }
        } else if ("q".equals(command) || "quit".equals(command)) {
            return session::quit;
        } else {
            throw new IllegalArgumentException("Command not recognized");
        }
    }

    private Iterator<String> toIterator(final String commandsString) {
        String[] split = commandsString.split("\\s+");
        return Arrays.asList(split).iterator();
    }

    public String siteMapFromNorthWestCornerHorizontally() {
        List<List<SiteSquareState>> map = session.getSiteStatus().fromNorthWestCorner();
        return map.stream().map(TextSessionAdapter::toString).collect(Collectors.joining("\n"));
    }

    public String commandList() {
        List<Command> commands = session.getCommands();
        return commands.stream().map(TextSessionAdapter::toString).collect(Collectors.joining(", "));
    }

    public Costs getCosts() {
        return session.getCosts();
    }
}
