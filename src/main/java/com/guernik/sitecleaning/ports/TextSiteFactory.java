package com.guernik.sitecleaning.ports;

import com.guernik.sitecleaning.domain.Site;
import com.guernik.sitecleaning.domain.SiteSquareState;

import static com.guernik.sitecleaning.domain.SiteSquareState.*;

/**
 * Factory class for {@link Site} based on the contents of a string.
 */
public class TextSiteFactory {

    /**
     * Builds a site based on a text representation of the site map.
     * The text representation must contain rows separate by new lines separator.
     * Each row must contain only the characters 'o', 't', 'r' or 'T', for a plain, tree, rocky or
     * protected tree square, respectively.
     * <p>
     * The site will have the first character of the first line in the North-West corner, the first
     * row in the {@param squaresString} will continue in West to East direction. Following rows in
     * {@code squaresString} will continue to define the site in North to South order.
     *
     * @param squaresString the String with the text representation of the site
     * @return a {@link Site} ready to be used
     * @throws IllegalArgumentException if {@param squaresString} contains an invalid character
     */
    public Site createSite(String squaresString) {
        String[] rows = squaresString.split("\\n");
        int northSouthLength = rows.length;
        int westEastLength = rows[0].length();
        final SiteSquareState[][] squareStates = new SiteSquareState[westEastLength][northSouthLength];
        for (int ns = 0; ns < northSouthLength; ns++) {
            for (int we = 0; we < westEastLength; we++) {
                squareStates[we][northSouthLength - 1 - ns] = toSiteSquareState(rows[ns].charAt(we));
            }
        }
        return new Site(squareStates);
    }

    private static SiteSquareState toSiteSquareState(final char c) {
        switch (c) {
            case 'o':
                return PLAIN;
            case 't':
                return CLEARABLE_TREE;
            case 'T':
                return PROTECTED_TREE;
            case 'r':
                return ROCKY;
            default:
                throw new IllegalArgumentException("Invalid square type: " + c);
        }
    }
}

